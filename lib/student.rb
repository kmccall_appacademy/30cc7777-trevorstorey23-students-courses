class Student
  attr_accessor :first_name, :last_name, :courses

  def initialize(first_name, last_name)
    @first_name = first_name
    @last_name = last_name
    @courses = []
  end

  def name
    "#{first_name} #{last_name}"
  end

  def enroll(new_course)
    raise "that course conflicts" if has_conflict?(new_course)
    unless @courses.include?(new_course)
      @courses << new_course
      new_course.students << self
    end
  end

  def course_load
    course_credits = Hash.new(0)
    @courses.each do |course|
      course_credits[course.department] += course.credits
    end
    course_credits
  end

  def has_conflict?(new_course)
    @courses.each do |course|
      if new_course.conflicts_with?(course)
        return true
      end
    end
    false
  end


end
